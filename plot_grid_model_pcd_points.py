#!/usr/bin/env python3.6

# https://www.bdmweather.com/2018/04/python-m-arcgisimage-basemap-options/
# https://waterprogramming.wordpress.com/2017/04/03/making-watershed-maps-in-python/

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import shapefile
import yaml
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from pfct import DefineGrid

with open('regiao.yml', 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

# 'plot_ce', 'plot_reg1', 'plot_reg2', 'plot_reg3', plot_reg4
reg = 'plot_ce'

x1 = cfg[reg]['x1']
x2 = cfg[reg]['x2']
y1 = cfg[reg]['y1']
y2 = cfg[reg]['y2']

fig, ax = plt.subplots(figsize=(20, 20))

m = Basemap(resolution='c', projection='cyl',
            llcrnrlat=y1, urcrnrlat=y2,
            llcrnrlon=x1, urcrnrlon=x2,
            suppress_ticks=True)

m.drawmeridians(np.arange(-160., 161., 0.5), labels=[0, 0, 0, 1],
                linewidth=0.001, fontsize=16, fontweight='bold')

m.drawparallels(np.arange(-90., 91., 0.5), labels=[1, 0, 0, 0],
                linewidth=0.001, fontsize=16, fontweight='bold')

m.readshapefile('shps/brazil_states', 'brazil_states',
                drawbounds=True, linewidth=0.8, color='k')

m.readshapefile('shps/mun_ceara_wgs84', 'mun_ceara_wgs84',
                drawbounds=True, linewidth=0.2, color='k')

# lat e lon da grade do modelo

lat_ori, lon_ori = DefineGrid.latloninfo('RSM97')

# ajuste na grade do modelo

deltalat = np.mean(np.diff(lat_ori)) / 2.
deltalon = np.mean(np.diff(lon_ori)) / 2.
lat = lat_ori - deltalat
lon = lon_ori - deltalon

# plot grade do modelo

lon_aux = np.linspace(min(lon), max(lon))
for i in lat:
    lat_aux = np.linspace(i, i)
    x, y = m(lon_aux, lat_aux)
    m.plot(x, y, linewidth=0.4, color='k', linestyle='-')

lat_aux = np.linspace(min(lat), max(lat))
for i in lon:
    lon_aux = np.linspace(i, i)
    x, y = m(lon_aux, lat_aux)
    m.plot(x, y, linewidth=0.4, color='k', linestyle='-')

# lat lon das pcds

arq_grp1 = 'pcds_grupo1.txt'
dados_grp1 = np.loadtxt(arq_grp1)

arq_grp2 = 'pcds_grupo2.txt'
dados_grp2 = np.loadtxt(arq_grp2)

arq_grp3 = 'pcds_grupo3.txt'
dados_grp3 = np.loadtxt(arq_grp3)

# plot pcds points

plt.plot(dados_grp1[:, 1], dados_grp1[:, 0], 'b.', markersize=14, label='REFERÊNCIA')
plt.plot(dados_grp2[:, 1], dados_grp2[:, 0], 'gp', markersize=14, label='INMET')
plt.plot(dados_grp3[:, 1], dados_grp3[:, 0], 'rX', markersize=10, label='DESATIVADAS')

polygon = Polygon([(x1, y1), (x1, y2), (x2, y2), (x2, y1)])
# x, y = polygon.exterior.xy
# plt.plot(x, y, color='#6699cc', alpha=0.7, linewidth=3, solid_capstyle='round', zorder=2)
# plt.show()

# plt nome municipios

shape_mun = shapefile.Reader('shps/mun_ceara_wgs84.shp')

for i in shape_mun.records():
    mun_nome = i[3]
    if mun_nome == 'Fortaleza':
        lon, lat = '-38.512', '-3.756'
    else:
        coords = i[10]
        lon, lat = coords[coords.find('(')+1:coords.find(')')].split()
    point = Point(float(lon), float(lat))
    check_polyin = polygon.contains(point)
    if check_polyin:
        if not reg == 'plot_ce':
            fontsize_text = 10
        else:
            fontsize_text = 8
        plt.text(float(lon), float(lat), mun_nome, fontsize=fontsize_text, fontweight='bold', horizontalalignment='center')
        print(float(lon), float(lat), mun_nome)

if reg == 'plot_reg3':
    loc_leg = 'lower left'
else:
    loc_leg = 'upper right'

plt.legend(loc=loc_leg, fontsize='x-large')

plt.xlabel('\n\nLONGITUDES', fontsize=24)
plt.ylabel('LATITUDES\n\n\n', fontsize=24)
plt.title('GEO-LOCALIZAÇÃO DAS PCDS\n', fontsize=30)
plt.savefig(f'pcds_{reg}.png', bbox_inches='tight', dpi=300)
plt.close()

# grp1 = 'pcds_grupo1.txt'
# with open(grp1, 'r') as myfile:
#     mylines = myfile.readlines()
#     x_pcd = []
#     y_pcd = []
#     for i in mylines:
#         x_pcd.append(i.split(' ')[1].strip())  # strip remove \n
#         y_pcd.append(i.split(' ')[0])
